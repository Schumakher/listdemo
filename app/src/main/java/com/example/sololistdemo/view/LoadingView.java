package com.example.sololistdemo.view;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.sololistdemo.R;
import com.example.sololistdemo.adapter.OnTryAgainClickListener;

public class LoadingView extends ConstraintLayout implements View.OnClickListener {

    private ProgressBar progressBar;
    private TextView errorTextView;
    private Button tryAgainButton;

    private OnTryAgainClickListener listener;

    public LoadingView(Context context) {
        super(context);
        init();
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View rootView = inflate(getContext(), R.layout.item_loading, this);

        progressBar = rootView.findViewById(R.id.loading_indeterminate_progress_bar);
        errorTextView = rootView.findViewById(R.id.loading_error_text_view);
        tryAgainButton = rootView.findViewById(R.id.loading_try_again_button);

        tryAgainButton.setOnClickListener(this);

        setLoadingMode(Mode.NONE);
    }

    public void setOnTryAgainClickListener(OnTryAgainClickListener listener) {
        this.listener = listener;
    }

    public void setLoadingMode(int loadingMode) {

        switch (loadingMode) {

            case Mode.NONE:
                setVisibility(GONE);
                break;

            case Mode.LOADING:
                errorTextView.setVisibility(GONE);
                progressBar.setVisibility(VISIBLE);
                tryAgainButton.setVisibility(GONE);
                setVisibility(VISIBLE);
                break;

            case Mode.ERROR:
                errorTextView.setVisibility(VISIBLE);
                progressBar.setVisibility(GONE);
                tryAgainButton.setVisibility(VISIBLE);
                setVisibility(VISIBLE);
                break;
        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() != R.id.loading_try_again_button) return;

        if (listener == null) return;

        listener.onTryAgainClick();
    }

    public class Mode {
        public static final int NONE = 0;
        public static final int LOADING = 1;
        public static final int ERROR = 2;
    }
}