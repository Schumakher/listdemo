package com.example.sololistdemo;

import android.app.Application;

import io.realm.Realm;

public class ProjectAppliation extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
    }
}