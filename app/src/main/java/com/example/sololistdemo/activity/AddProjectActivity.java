package com.example.sololistdemo.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.sololistdemo.Constants;
import com.example.sololistdemo.R;
import com.example.sololistdemo.SomeNetworkManager;
import com.example.sololistdemo.State;
import com.example.sololistdemo.model.Product;
import com.example.sololistdemo.view.LoadingView;
import com.example.sololistdemo.viewModel.AddProjectViewModel;

public class AddProjectActivity extends AppCompatActivity implements View.OnClickListener {

    private LoadingView loadingView;
    private TextInputEditText titleEditText;
    private TextInputEditText descriptionEditText;
    private Button saveButton;
    private View viewGroup;

    private AddProjectViewModel viewModel;
    private boolean isEditMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_project);

        viewGroup = findViewById(R.id.view_group);
        loadingView = findViewById(R.id.loading_view);
        titleEditText = findViewById(R.id.title_edit_text);
        descriptionEditText = findViewById(R.id.description_edit_text);
        saveButton = findViewById(R.id.save_button);
        saveButton.setOnClickListener(this);

        subscribeToModel();
    }

    private void subscribeToModel() {

        viewModel = ViewModelProviders
                .of(this)
                .get(AddProjectViewModel.class);

        viewModel.initialize(getIntent().getIntExtra(Constants.EXTRA_PRODUCT_ID, -1));

        viewModel.setSomeNetworkManager(new SomeNetworkManager(this));

        viewModel
                .getStateLieData()
                .observe(this, state -> {

                    switch (state) {

                        case State.LOADING:
                            loadingView.setLoadingMode(LoadingView.Mode.LOADING);
                            viewGroup.setVisibility(View.GONE);
                            break;

                        case State.ADDED:
                            finish();
                            break;

                        case State.ERROR:
                            loadingView.setLoadingMode(LoadingView.Mode.ERROR);
                            viewGroup.setVisibility(View.GONE);
                            break;

                        case State.NONE:
                            viewGroup.setVisibility(View.VISIBLE);
                            loadingView.setLoadingMode(LoadingView.Mode.NONE);
                            break;

                        case State.SAVING:
                            viewGroup.setVisibility(View.VISIBLE);
                            loadingView.setLoadingMode(LoadingView.Mode.LOADING);
                            break;
                    }
                });

        viewModel
                .getProductLiveData()
                .observe(this, product -> {
                    titleEditText.setText(product.getTitle());
                    descriptionEditText.setText(product.getDescription());
                });

        viewModel.reload();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() != R.id.save_button) return;

        Product product = new Product(titleEditText.getText().toString(), descriptionEditText.getText().toString());
        viewModel.saveProduct(product);
    }
}