package com.example.sololistdemo.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.sololistdemo.Constants;
import com.example.sololistdemo.R;
import com.example.sololistdemo.SomeNetworkManager;
import com.example.sololistdemo.State;
import com.example.sololistdemo.view.LoadingView;
import com.example.sololistdemo.viewModel.ProductDetailsViewModel;

public class ProductDetailsActivity extends AppCompatActivity {

    private View detailsView;
    private TextView titleTextView;
    private TextView descriptionTextView;

    private LoadingView loadingView;
    private MenuItem removeButton;

    private ProductDetailsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        detailsView = findViewById(R.id.details_view_group);
        titleTextView = findViewById(R.id.title_text_view);
        descriptionTextView = findViewById(R.id.description_text_view);

        loadingView = findViewById(R.id.loading_view);
        loadingView.setOnTryAgainClickListener(() -> viewModel.reload());

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        subscribeToModel();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_remove, menu);
        removeButton = menu.findItem(R.id.action_remove);
        removeButton.setEnabled(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_remove:
                viewModel.remove();
                return true;

            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void subscribeToModel() {

        viewModel = ViewModelProviders
                .of(this)
                .get(ProductDetailsViewModel.class);

        viewModel.initialize(getIntent().getIntExtra(Constants.EXTRA_PRODUCT_ID, -1));
        viewModel.setSomeNetworkManager(new SomeNetworkManager(this));

        viewModel
                .getProductLiveData()
                .observe(this, product -> {
                    titleTextView.setText(product.getTitle());
                    descriptionTextView.setText(product.getDescription());
                });

        viewModel
                .getStateLieData()
                .observe(this, state -> {

                    if (removeButton != null) {
                        removeButton.setEnabled(state == State.NONE);
                    }

                    detailsView.setVisibility(state == State.NONE ? View.VISIBLE : View.INVISIBLE);

                    switch (state) {

                        case State.LOADING:
                            //Be sure to SHOW loading after all UI changes so user will not notice them /Sch
                            loadingView.setLoadingMode(LoadingView.Mode.LOADING);
                            break;

                        case State.NONE:
                            //Be sure to HIDE loading after all UI changes so user will not notice them /Sch
                            loadingView.setLoadingMode(LoadingView.Mode.NONE);
                            break;

                        case State.OFFLINE:
                        case State.ERROR:
                            loadingView.setLoadingMode(LoadingView.Mode.ERROR);
                            break;

                        case State.DELETED:
                            finish();
                            break;
                    }
                });

        viewModel.reload();
    }
}