package com.example.sololistdemo.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.MenuItem;

import com.example.sololistdemo.R;
import com.example.sololistdemo.fragment.ProductListFragment;
import com.example.sololistdemo.model.Product;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private BottomNavigationView bottomNavigationView;
    private SparseArray<Fragment> fragmentSparseArray;
    private Fragment active;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.insertOrUpdate(getData());
        realm.commitTransaction();

        bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        fragmentSparseArray = new SparseArray<>();

        processPageTitle(bottomNavigationView.getSelectedItemId());
//        loadFragment(getCorrespondingFragment(bottomNavigationView.getSelectedItemId()));

        fragmentSparseArray.put(R.id.action_home, new ProductListFragment());
        fragmentSparseArray.put(R.id.action_shop, new ProductListFragment());
        fragmentSparseArray.put(R.id.action_cart, new ProductListFragment());
        fragmentSparseArray.put(R.id.action_bookmark, new ProductListFragment());


        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_container, fragmentSparseArray.get(R.id.action_bookmark), String.valueOf(R.id.action_bookmark))
                .hide(fragmentSparseArray.get(R.id.action_bookmark))
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_container, fragmentSparseArray.get(R.id.action_cart), String.valueOf(R.id.action_cart))
                .hide(fragmentSparseArray.get(R.id.action_cart))
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_container, fragmentSparseArray.get(R.id.action_shop), String.valueOf(R.id.action_shop))
                .hide(fragmentSparseArray.get(R.id.action_shop))
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_container, fragmentSparseArray.get(R.id.action_home), String.valueOf(R.id.action_home))
                .commit();

        active = fragmentSparseArray.get(R.id.action_home);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
//        loadFragment(getCorrespondingFragment(menuItem.getItemId()));
//        showFragment(menuItem.getItemId());
        replace(menuItem.getItemId());
        return processPageTitle(menuItem.getItemId());
    }

    private void replace(int itemId) {
        getSupportFragmentManager()
                .beginTransaction()
                .hide(active)
                .show(fragmentSparseArray.get(itemId))
                .commit();

        active = fragmentSparseArray.get(itemId);
    }

    private boolean processPageTitle(int selectedItemId) {

        switch (selectedItemId) {

            case R.id.action_home:
                getSupportActionBar().setTitle(R.string.home);
                return true;

            case R.id.action_shop:
                getSupportActionBar().setTitle(R.string.shop);
                return true;

            case R.id.action_cart:
                getSupportActionBar().setTitle(R.string.cart);
                return true;

            case R.id.action_bookmark:
                getSupportActionBar().setTitle(R.string.saved);
                return true;

            default:
                return false;
        }
    }

    private Fragment getCorrespondingFragment(int selectedItemId) {

        Fragment fragment = fragmentSparseArray.get(selectedItemId);

        if (fragment == null) {
            fragment = new ProductListFragment();
            fragmentSparseArray.put(selectedItemId, fragment);
        }

        return fragment;
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
//        transaction.addToBackStack(null);
        transaction.commit();
    }

    private List<Product> getData() {

        List<Product> data = new ArrayList<>();
        data.add(new Product(0, "Zero", "This is the very first product"));
        data.add(new Product(1, "Second", "Second coming"));
        data.add(new Product(2, "Third", "Oh baby a triple!"));
        data.add(new Product(3, "Quad", "Something like processor cores description"));
        data.add(new Product(4, "Penta", "You must be hard enough to o penta and stay alive"));
        data.add(new Product(5, "Hexa", "First Linus long last day off results as Git"));
        data.add(new Product(6, "Hepta", "Can You please be so kindly to describe me the whole spectre of party hard? Or maybe You can tell me a joke."));
        data.add(new Product(7, "Octo", "Octopus cartoon?"));
        data.add(new Product(8, "Ennea", "Seriously?! Man, just enough kidding me."));
        data.add(new Product(9, "Deca", "And the second Linus long last day off results as much hugging hugs in that huggy linux source codes"));
        data.add(new Product(10, "Hendeca", "Hands up. Baby, hands up. Gimme your heart, gimme, gimme your heart"));
        data.add(new Product(11, "Dodeca", "Interesting. Maybe we can invent a new sport discipline: Dodecathlon"));
        data.add(new Product(12, "Tria(kai)deca", "Oh god. What going on here?!"));
        data.add(new Product(13, "Tessara(kai)deca", "And the Raven, never flitting, still is sitting, still is sitting\n" +
                "On the pallid bust of Pallas just above my chamber door;\n" +
                "And his eyes have all the seeming of a demon’s that is dreaming,\n" +
                "And the lamp-light o’er him streaming throws his shadow on the floor;\n" +
                "And my soul from out that shadow that lies floating on the floor\n" +
                "               Shall be lifted—nevermore!"));
        data.add(new Product(14, "Pente(kai)deca", "Try to come up with a new quote by yourself."));
        data.add(new Product(15, "Hexa(kai)deca", "Omae wa mou shindeiru"));
        return data;
    }
}