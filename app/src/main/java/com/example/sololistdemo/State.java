package com.example.sololistdemo;

public class State {
    public static final int UNDEFINED = -1;

    public static final int NONE = 0;
    public static final int LOADING = 1;
    public static final int ERROR = 2;
    public static final int OFFLINE = 3;
    public static final int REFRESH = 4;
    public static final int MORE = 5;
    public static final int REACHED_END = 6;
    public static final int DELETED = 7;
    public static final int ADDED = 8;
    public static final int SAVING = 9;
}