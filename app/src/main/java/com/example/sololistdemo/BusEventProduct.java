package com.example.sololistdemo;

import android.support.annotation.Nullable;

import com.example.sololistdemo.model.Product;

public class BusEventProduct {

    @Nullable
    private Product product;
    private int event;

    public BusEventProduct(@Nullable Product product, int event) {
        this.product = product;
        this.event = event;
    }

    public BusEventProduct(int event) {
        this.event = event;
    }

    @Nullable
    public Product getProduct() {
        return product;
    }

    public int getEvent() {
        return event;
    }

    public static class Event {
        public static final int DELETE = 0;
        public static final int CREATE = 1;
        public static final int UPDATE = 2;
    }
}