package com.example.sololistdemo.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.sololistdemo.adapter.viewHolder.LoadingViewHolder;
import com.example.sololistdemo.adapter.viewHolder.ProductViewHolder;
import com.example.sololistdemo.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter implements OnItemClickListener, OnTryAgainClickListener {

    private List<Product> productList;
    private OnItemProductClickListener listener;

    private int loadingMode;

    public ProductAdapter(OnItemProductClickListener listener) {
        this.listener = listener;
        productList = new ArrayList<>();

        setHasStableIds(true);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        if (viewType == ViewType.LOADING) {
            return LoadingViewHolder.initialize(viewGroup, this);
        }

        return ProductViewHolder.initialize(viewGroup, this);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int index) {

        if (getItemViewType(index) == ViewType.LOADING) {
            ((LoadingViewHolder) viewHolder).setupUi(loadingMode);
            return;
        }

        ProductViewHolder productViewHolder = (ProductViewHolder) viewHolder;
        productViewHolder.setupUi(productList.get(index));
        productViewHolder.updateDivider(index != productList.size() - 1);
    }

    @Override
    public int getItemCount() {
        int count = productList.size();

        if (loadingMode != LoadingViewHolder.Mode.NONE) {
            count++;
        }

        return count;
    }

    @Override
    public long getItemId(int position) {

        if (getItemViewType(position) == ViewType.LOADING) {
            return Integer.MAX_VALUE;
        }

        return productList.get(position).getId();
    }

    public void setLoadingMode(int loadingMode) {

        //no need of extra updating /Sch
        if (this.loadingMode == loadingMode) return;

        int oldLoadingMode = this.loadingMode;
        this.loadingMode = loadingMode;

        if (loadingMode == LoadingViewHolder.Mode.NONE) {
            notifyItemRemoved(productList.size());
            return;
        }

        //case when loading was in rest and now must be shown /Sch
        if (oldLoadingMode == LoadingViewHolder.Mode.NONE) {
            notifyItemInserted(productList.size());
            return;
        }

        notifyItemChanged(productList.size());
    }

    @Override
    public int getItemViewType(int position) {

        if (position == productList.size()) {
            return ViewType.LOADING;
        }

        return ViewType.CONTENT;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;

//        if (!productList.isEmpty()) {
//            for (int index = 0; index < 5; index++) {
//                this.productList.add(productList.get(index));
//            }
//        }
        notifyDataSetChanged();
    }

    @Override
    public void onItemClick(RecyclerView.ViewHolder viewHolder) {
        int position = viewHolder.getAdapterPosition();

        if (position == RecyclerView.NO_POSITION) return;

        listener.onItemProductClick(productList.get(position));
    }

    @Override
    public void onEdit(RecyclerView.ViewHolder viewHolder) {
        int position = viewHolder.getAdapterPosition();

        if (position == RecyclerView.NO_POSITION) return;

        listener.onItemProductEdit(productList.get(position));
    }

    @Override
    public void onTryAgainClick() {
        listener.onListTryAgainClick();
    }

    public interface OnItemProductClickListener {
        void onItemProductClick(Product product);
        void onItemProductEdit(Product product);
        void onListTryAgainClick();
    }

    private class ViewType {
        static final int CONTENT = 0;
        static final int LOADING = 1;
    }
}