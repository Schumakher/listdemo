package com.example.sololistdemo.adapter;

import android.support.v7.widget.RecyclerView;

public interface OnItemClickListener {
    void onItemClick(RecyclerView.ViewHolder viewHolder);
    void onEdit(RecyclerView.ViewHolder viewHolder);
}