package com.example.sololistdemo.adapter.viewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sololistdemo.R;
import com.example.sololistdemo.adapter.OnItemClickListener;
import com.example.sololistdemo.model.Product;

public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ImageView iconImageView;
    private TextView titleTextView;
    private TextView descriptionTextView;
    private View divider;

    private OnItemClickListener listener;

    public static ProductViewHolder initialize(ViewGroup parent, OnItemClickListener listener) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ProductViewHolder(itemView, listener);
    }

    private ProductViewHolder(@NonNull View itemView, OnItemClickListener listener) {
        super(itemView);
        this.listener = listener;

        iconImageView = itemView.findViewById(R.id.icon_image_view);
        titleTextView = itemView.findViewById(R.id.title_text_view);
        descriptionTextView = itemView.findViewById(R.id.description_text_view);
        divider = itemView.findViewById(R.id.divider);

        itemView.findViewById(R.id.container).setOnClickListener(this);
        itemView.findViewById(R.id.edit_button).setOnClickListener(this);
    }

    public void setupUi(Product product) {

        titleTextView.setText(product.getTitle());
        descriptionTextView.setText(product.getDescription());
    }

    public void updateDivider(boolean show) {
        divider.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.edit_button) {
            listener.onEdit(this);
            return;
        }

        listener.onItemClick(this);
    }
}