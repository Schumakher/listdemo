package com.example.sololistdemo.adapter;

public interface OnTryAgainClickListener {
    void onTryAgainClick();
}