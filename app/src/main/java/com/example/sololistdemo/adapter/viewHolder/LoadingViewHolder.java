package com.example.sololistdemo.adapter.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.sololistdemo.R;
import com.example.sololistdemo.adapter.OnTryAgainClickListener;

public class LoadingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ProgressBar progressBar;
    private TextView errorTextView;
    private Button tryAgainButton;

    private OnTryAgainClickListener listener;

    public static LoadingViewHolder initialize(ViewGroup parent, OnTryAgainClickListener listener) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
        return new LoadingViewHolder(itemView, listener);
    }

    private LoadingViewHolder(View itemView, OnTryAgainClickListener listener) {
        super(itemView);
        this.listener = listener;

        progressBar = itemView.findViewById(R.id.loading_indeterminate_progress_bar);
        errorTextView = itemView.findViewById(R.id.loading_error_text_view);

        tryAgainButton = itemView.findViewById(R.id.loading_try_again_button);
        tryAgainButton.setOnClickListener(this);
    }

    public void setupUi(int loadingMode) {

        switch (loadingMode){

            case Mode.NONE:
                itemView.setVisibility(View.GONE);
                break;

            case Mode.LOADING:
                progressBar.setVisibility(View.VISIBLE);
                errorTextView.setVisibility(View.GONE);
                tryAgainButton.setVisibility(View.GONE);
                break;

            case Mode.ERROR:
                progressBar.setVisibility(View.GONE);
                errorTextView.setVisibility(View.VISIBLE);
                tryAgainButton.setVisibility(View.VISIBLE);
                break;
        }

        itemView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() != R.id.loading_try_again_button) return;

        listener.onTryAgainClick();
    }

    public class Mode {
        public static final int NONE = 0;
        public static final int LOADING = 1;
        public static final int ERROR = 2;
    }
}