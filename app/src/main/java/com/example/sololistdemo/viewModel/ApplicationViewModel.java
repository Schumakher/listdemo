package com.example.sololistdemo.viewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.sololistdemo.SomeNetworkManager;
import com.example.sololistdemo.State;

import io.realm.Realm;

public abstract class ApplicationViewModel extends ViewModel {

    final int REQUEST_CHUNK_COUNT = 7;

    int lastRequestIndex;
    boolean hasReachedEnd;

    Realm realm;

    //Just a temp solution to determine network state /Sch
    SomeNetworkManager someNetworkManager;

    private MutableLiveData<Integer> stateLieData;

    ApplicationViewModel() {
        stateLieData = new MutableLiveData<>();
        stateLieData.setValue(State.UNDEFINED);

        realm = Realm.getDefaultInstance();
    }

    public void setSomeNetworkManager(SomeNetworkManager someNetworkManager) {
        this.someNetworkManager = someNetworkManager;
    }

    public MutableLiveData<Integer> getStateLieData() {
        return stateLieData;
    }

    public abstract void reload();
}
