package com.example.sololistdemo.viewModel;

import android.arch.lifecycle.MutableLiveData;
import android.os.Handler;

import com.example.sololistdemo.BusEventProduct;
import com.example.sololistdemo.State;
import com.example.sololistdemo.model.Product;

import org.greenrobot.eventbus.EventBus;

public class ProductDetailsViewModel extends ApplicationViewModel {

    private int productId;
    private MutableLiveData<Product> productLiveData;

    public ProductDetailsViewModel() {
        productLiveData = new MutableLiveData<>();
    }

    public void initialize(int productId) {
        this.productId = productId;
    }

    public MutableLiveData<Product> getProductLiveData() {
        return productLiveData;
    }

    private void makeGetItemProductRequest() {

        if (!setStateAndProceed()) return;

        new Handler().postDelayed(() -> {

            Product product = realm
                    .where(Product.class)
                    .equalTo("id", productId)
                    .findFirst();

            //if request failed, set state to error /Sch

            //Don't forget to copy from realm if You want standalone object instead of reference /Sch
            productLiveData.setValue(realm.copyFromRealm(product));
            getStateLieData().setValue(State.NONE);

        }, 2000);
    }

    public void remove() {
        makeRemoveRequest();
    }

    private void makeRemoveRequest() {

        if (!setStateAndProceed()) return;

        new Handler().postDelayed(() -> {

            Product product = realm
                    .where(Product.class)
                    .equalTo("id", productId)
                    .findFirst();

            Product realProductObject = realm.copyFromRealm(product);

            realm.beginTransaction();
            product.deleteFromRealm();
            realm.commitTransaction();

            //if request failed, set state to DELETED error /Sch

            EventBus
                    .getDefault()
                    .post(new BusEventProduct(realProductObject, BusEventProduct.Event.DELETE));

            getStateLieData().setValue(State.DELETED);

        }, 2000);
    }

    private boolean setStateAndProceed() {
        if (!someNetworkManager.isNetworkAvailable()) {
            getStateLieData().setValue(State.OFFLINE);
            return false;
        }

        //Seems this is more complicated work, thus do it after connection check /Sch
        if (getStateLieData().getValue() == State.LOADING /*&& oldRequest == newRequest*/)
            return false;

        getStateLieData().setValue(State.LOADING);
        return true;
    }

    @Override
    public void reload() {
        makeGetItemProductRequest();
    }
}