package com.example.sololistdemo.viewModel;

import android.arch.lifecycle.MutableLiveData;
import android.os.Handler;

import com.example.sololistdemo.BusEventProduct;
import com.example.sololistdemo.State;
import com.example.sololistdemo.model.Product;

import org.greenrobot.eventbus.EventBus;

import io.realm.Sort;

public class AddProjectViewModel extends ApplicationViewModel {

    private int productId;
    private MutableLiveData<Product> productLiveData;

    public AddProjectViewModel() {
        productLiveData = new MutableLiveData<>();
    }

    public void initialize(int productId) {
        this.productId = productId;
    }

    public void saveProduct(Product productBody) {
        makeSaveProductRequest(productBody);
    }

    private void makeSaveProductRequest(Product productBody) {

        if (!setStateAndProceed(true)) return;

        if (productId == -1) {
            Product product = realm
                    .where(Product.class)
                    .sort("id", Sort.DESCENDING)
                    .findFirst();

            //in fact object with its real id must be retrieved as a response from server/Sch
            productBody.setId(product.getId() + 1);
        } else {
            productBody.setId(productId);
        }

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(productBody);
        realm.commitTransaction();

        EventBus
                .getDefault()
                .post(new BusEventProduct(productBody, productId == -1 ? BusEventProduct.Event.CREATE : BusEventProduct.Event.UPDATE));

        getStateLieData().setValue(State.ADDED);
    }

    private boolean setStateAndProceed(boolean save) {
        if (!someNetworkManager.isNetworkAvailable()) {
            getStateLieData().setValue(State.OFFLINE);
            return false;
        }

        //Seems this is more complicated work, thus do it after connection check /Sch
        if (getStateLieData().getValue() == State.LOADING /*&& oldRequest == newRequest*/)
            return false;

        getStateLieData().setValue(save ? State.SAVING : State.LOADING);
        return true;
    }

    private void makeGetItemProductRequest() {

        if (!setStateAndProceed(false)) return;

        new Handler().postDelayed(() -> {

            Product product = realm
                    .where(Product.class)
                    .equalTo("id", productId)
                    .findFirst();

            //if request failed, set state to error /Sch

            //Don't forget to copy from realm if You want standalone object instead of reference /Sch
            productLiveData.setValue(realm.copyFromRealm(product));
            getStateLieData().setValue(State.NONE);

        }, 2000);
    }

    public MutableLiveData<Product> getProductLiveData() {
        return productLiveData;
    }

    @Override
    public void reload() {

        if (productId != -1) {
            makeGetItemProductRequest();
        }
    }
}