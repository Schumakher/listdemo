package com.example.sololistdemo.viewModel;

import android.arch.lifecycle.MutableLiveData;
import android.os.Handler;

import com.example.sololistdemo.BusEventProduct;
import com.example.sololistdemo.State;
import com.example.sololistdemo.model.Product;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.RealmResults;

public class ProductListViewModel extends ApplicationViewModel {

    private MutableLiveData<List<Product>> productListLiveData;

    public ProductListViewModel() {
        productListLiveData = new MutableLiveData<>();
        productListLiveData.setValue(new ArrayList<>());

        EventBus
                .getDefault()
                .register(this);
    }

    public MutableLiveData<List<Product>> getProductListLiveData() {
        return productListLiveData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();

        EventBus
                .getDefault()
                .unregister(this);
    }

    public void reload() {
        reset(true);
        makeGetProjectsRequest(false);
    }

    public void refresh() {
        reset(false);
        makeGetProjectsRequest(true);
    }

    public void loadMore() {
        makeGetProjectsRequest(false);
    }

    public boolean hasData() {
        return productListLiveData.getValue() != null && !productListLiveData.getValue().isEmpty();
    }

    private void makeGetProjectsRequest(boolean refresh) {

        if (!setStateAndProceed(refresh)) return;

        new Handler().postDelayed(() -> {

            RealmResults<Product> productsResult = realm
                    .where(Product.class)
                    .sort("title")
//                    .between("id", lastRequestIndex, lastRequestIndex + REQUEST_CHUNK_COUNT)
                    .findAll();

            List<Product> products = new ArrayList<>();
            if (lastRequestIndex + REQUEST_CHUNK_COUNT > productsResult.size()) {
                productsResult.subList(lastRequestIndex, productsResult.size());
            } else {
                products = realm.copyFromRealm(productsResult.subList(lastRequestIndex, lastRequestIndex + REQUEST_CHUNK_COUNT));
            }

            //if request failed, set state to error /Sch


            //Don't forget to copy from realm if You want standalone objects instead of references /Sch
            List<Product> current = productListLiveData.getValue();

            if (refresh) {
                current = new ArrayList<>();
                reset(false);
            }

            hasReachedEnd = products.size() < REQUEST_CHUNK_COUNT;
            lastRequestIndex += products.size();

            List<Product> duplicateProducts = new ArrayList<>();

            for (int index = 0; index < products.size(); index++) {
                for (Product itemProduct : current) {

                    if (products.get(index).getId() == itemProduct.getId()) {
                        duplicateProducts.add(products.get(index));
                    }
                }
            }

            List<Product> fetchedList = products;
            fetchedList.removeAll(duplicateProducts);

            current.addAll(fetchedList);
            productListLiveData.setValue(current);

            getStateLieData().setValue(hasReachedEnd ? State.REACHED_END : State.NONE);

        }, 2000);
    }

    private boolean setStateAndProceed(boolean refresh) {

        if (hasReachedEnd) return false;

        if (!someNetworkManager.isNetworkAvailable()) {
            getStateLieData().setValue(State.OFFLINE);
            return false;
        }

        //Seems this is more complicated work, thus do it after connection check /Sch
        if (getStateLieData().getValue() == State.LOADING /*&& oldRequest == newRequest*/)
            return false;

        if (refresh) {
            getStateLieData().setValue(State.REFRESH);
            return true;
        }

        getStateLieData().setValue(lastRequestIndex > 0 ? State.MORE : State.LOADING);
        return true;
    }

    private void reset(boolean hard) {

        if (hard) {
            productListLiveData.setValue(new ArrayList<>());
        }

        hasReachedEnd = false;
        lastRequestIndex = 0;
    }

    @Subscribe
    public void onProductEvent(BusEventProduct eventProduct) {
        List<Product> current = productListLiveData.getValue();

        switch (eventProduct.getEvent()) {

            case BusEventProduct.Event.DELETE:
                current.remove(eventProduct.getProduct());
                productListLiveData.setValue(current);
                break;

            case BusEventProduct.Event.CREATE:
                current.add(0, eventProduct.getProduct());
                productListLiveData.setValue(current);
                break;

            case BusEventProduct.Event.UPDATE:

                int changeIndex = -1;
                for (int index = 0; index < current.size(); index++) {
                    if (current.get(index).getId() == eventProduct.getProduct().getId()) {
                        changeIndex = index;
                        break;
                    }
                }

                current.set(changeIndex, eventProduct.getProduct());
                productListLiveData.setValue(current);
                break;
        }
    }
}