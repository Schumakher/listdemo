package com.example.sololistdemo.fragment;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sololistdemo.Constants;
import com.example.sololistdemo.R;
import com.example.sololistdemo.SomeNetworkManager;
import com.example.sololistdemo.State;
import com.example.sololistdemo.activity.AddProjectActivity;
import com.example.sololistdemo.activity.ProductDetailsActivity;
import com.example.sololistdemo.adapter.OnTryAgainClickListener;
import com.example.sololistdemo.adapter.ProductAdapter;
import com.example.sololistdemo.adapter.viewHolder.LoadingViewHolder;
import com.example.sololistdemo.model.Product;
import com.example.sololistdemo.view.LoadingView;
import com.example.sololistdemo.viewModel.ProductListViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductListFragment extends Fragment implements ProductAdapter.OnItemProductClickListener, OnTryAgainClickListener, View.OnClickListener {

    private SwipeRefreshLayout productSwipeRefreshLayout;
    private RecyclerView productRecyclerView;
    private LoadingView loadingView;
    private View noResultsView;
    private FloatingActionButton addFab;

    private ProductListViewModel viewModel;
    private ProductAdapter productAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders
                .of(this)
                .get(ProductListViewModel.class);

        viewModel.setSomeNetworkManager(new SomeNetworkManager(getContext()));

        productAdapter = new ProductAdapter(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_product_list, container, false);

        productRecyclerView = rootView.findViewById(R.id.product_recycler_view);
        productRecyclerView.setAdapter(productAdapter);
        productRecyclerView.addOnScrollListener(onScrollListener);

        loadingView = rootView.findViewById(R.id.loading_view);
        loadingView.setOnTryAgainClickListener(this);

        noResultsView = rootView.findViewById(R.id.no_results_group);

        addFab = rootView.findViewById(R.id.add_fab);
        addFab.setOnClickListener(this);

        productSwipeRefreshLayout = rootView.findViewById(R.id.product_swipe_refresh_layout);
        productSwipeRefreshLayout.setOnRefreshListener(() -> viewModel.refresh());

        subscribeToModel();
        return rootView;
    }

    private void subscribeToModel() {

        viewModel
                .getProductListLiveData()
                .observe(getViewLifecycleOwner(), products -> {
//                    productRecyclerView.setVisibility(products.isEmpty() ? View.INVISIBLE : View.VISIBLE);
                    productAdapter.setProductList(products);
                });

        viewModel
                .getStateLieData()
                .observe(getViewLifecycleOwner(), state -> {

                    noResultsView.setVisibility((state == State.NONE || state == State.REACHED_END) &&
                            !viewModel.hasData() ? View.VISIBLE : View.GONE);

                    switch (state) {

                        case State.LOADING:
                            //Be sure to SHOW loading after all UI changes so user will not notice them /Sch
                            loadingView.setLoadingMode(LoadingView.Mode.LOADING);
                            productRecyclerView.setVisibility(View.INVISIBLE);
                            break;

                        case State.NONE:
//                            noResultsView.setVisibility(viewModel.hasData() ? View.GONE : View.VISIBLE);
                            productRecyclerView.setVisibility(View.VISIBLE);
                            productSwipeRefreshLayout.setRefreshing(false);
                            productAdapter.setLoadingMode(LoadingViewHolder.Mode.NONE);

                            //Be sure to HIDE loading after all UI changes so user will not notice them /Sch
                            loadingView.setLoadingMode(LoadingView.Mode.NONE);
                            break;

                        case State.OFFLINE:
                        case State.ERROR:
                            if (productSwipeRefreshLayout.isRefreshing()) {
                                productSwipeRefreshLayout.setRefreshing(false);
                                break;
                            }

                            if (viewModel.hasData()) {
                                productAdapter.setLoadingMode(LoadingViewHolder.Mode.ERROR);
                                break;
                            }

                            loadingView.setLoadingMode(LoadingView.Mode.ERROR);
                            break;

                        case State.REFRESH:
                            break;

                        case State.MORE:
                            productAdapter.setLoadingMode(LoadingViewHolder.Mode.LOADING);
                            break;

                        case State.REACHED_END:
                            productAdapter.setLoadingMode(LoadingViewHolder.Mode.NONE);
                            productSwipeRefreshLayout.setRefreshing(false);
                            loadingView.setLoadingMode(LoadingView.Mode.NONE);
//                            noResultsView.setVisibility(viewModel.hasData() ? View.GONE : View.VISIBLE);
                            break;
                    }
                });

        viewModel.reload();
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            if (layoutManager.findLastVisibleItemPosition() < (viewModel.getProductListLiveData().getValue().size() - 3)) {
                return;
            }

            viewModel.loadMore();
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        productRecyclerView.setAdapter(null);
        productRecyclerView.removeOnScrollListener(onScrollListener);
    }

    @Override
    public void onItemProductClick(Product product) {
        startActivity(new Intent(getContext(), ProductDetailsActivity.class).putExtra(Constants.EXTRA_PRODUCT_ID, product.getId()));
    }

    @Override
    public void onItemProductEdit(Product product) {
        startActivity(new Intent(getContext(), AddProjectActivity.class).putExtra(Constants.EXTRA_PRODUCT_ID, product.getId()));
    }

    @Override
    public void onListTryAgainClick() {
        viewModel.loadMore();
    }

    @Override
    public void onTryAgainClick() {
        viewModel.reload();
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(getContext(), AddProjectActivity.class));
    }
}